add_library('controlP5')

#ControlP5 cp5

#variables globales de los sliders
a = 1
b = 1
n1 = 10
n2 = 6
n3 = 6

# Proporcion de la pantalla dedicada a los controles
tamGuiX= 0.150

#Sliders globales
sn3 = None
sn2 = None
sn1 = None
btnagap = None

def setup():
    global sn3,sn2,sn1,btnagap
    fullScreen()

    h = int(height * .03)
    w = int(width * tamGuiX)
    sep = int( height * .04)
    background(0)
    stroke(255)
    smooth()

    cp5 = ControlP5(this)  
    
    p = createFont("Verdana",9)
    cp5.setFont(p)

    cp5.setColorForeground(0xffaa0000)
    cp5.setColorBackground(0xff660000)
    cp5.setColorValueLabel(0xffff88ff)
    cp5.setColorActive(0xffff0000)
  
    sn1 = Slider(cp5,"N1")
    sn1.setPosition(10,sep*5).setSize(w,h).setRange(0,25).setValue(10).setNumberOfTickMarks(101).snapToTickMarks(True).showTickMarks(False)
    sn2 = Slider(cp5,"N2")
    sn2.setPosition(10,sep*6).setSize(w,h).setRange(0,25).setValue(6).setNumberOfTickMarks(101).snapToTickMarks(True).showTickMarks(False)
    sn3 = Slider(cp5,"N3")
    sn3.setPosition(10,sep*7).setSize(w,h).setRange(0,25).setValue(6).setNumberOfTickMarks(101).snapToTickMarks(True).showTickMarks(False)
 
    btnagap = Button(cp5,"AGAP")
    btnagap.setPosition(10,sep*24).setSize(w,h)
    cp5.addCallback(controlDelEvento)

def draw():
    global n1,n2,n3,tamGuiX, agap,btnagap


def controlDelEvento(theEvent):
    global n1,n2,n3
#    print "principio del evento"
        
    if(theEvent.getAction()==ControlP5.ACTION_RELEASE) :
#        print "se ha pulsado un control"
        ctrl = theEvent.getController()

        if(ctrl.getName()=="N1"):
            n1 = theEvent.getController().getValue()
        if(ctrl.getName()=="N2"):
            n2 = theEvent.getController().getValue()
        if(ctrl.getName()=="N3"):
            n3 = theEvent.getController().getValue()

        if(ctrl.getName()=="AGAP"):
            #llama a una funcion
            setValores(n1,n2,n3)
            
    background(0)
   
  
def setValores(setn1,setn2,setn3):
    global n1,n2,n3,sn3,sn2,sn1
    n1= setn1
    n2= setn2
    n3= setn3
    sn1.setValue(setn1)
    sn2.setValue(setn2)
    sn3.setValue(setn3)
