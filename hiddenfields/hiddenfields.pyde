# Basado en los fondos de http://www.interaliamag.org/interviews/hidden-fields-danceroom-spectroscopy/
#

#Variables globales
origenX = None   #origen de la luz
origenY = None
tamanoLineas = 100 #lineas de 100 puntos
maxDist = None
numColores = 12

def setup():
    
    global maxDist,origenX,origenY
    size(800,600)
    background(0)
    colorMode(HSB)
    origenX = random(width)
    origenY = random(height)
#    ellipse(origenX,origenY,50,50)
    maxDist = dist(0,0,width,height)
    
def draw():
    # x,y,x1,y1 principio y fin de la línea
    # angulo, tamano  es el de las lineas
    
    x = random(width)
    y = random(height)
    
    angulo = random(TWO_PI)
           
    linea (x,y,angulo,tamanoLineas)
    

def linea(x0,y0,ang,tam):
    # Dibuja una linea por puntos (para poder variar el color en ella)
    a = 0
    while(a<tam):
        x = x0 + a*cos(ang)
        y = y0 + a*sin(ang)
        punto(x,y)
        a = a+1

def punto(x,y):
    #dibuja un punto del color que determine segun su posición
    incColor = maxDist/numColores
    d = abs(dist(origenX,origenY,x,y))
    h = int(d /incColor)*256/numColores
    s = 200 #saturacion
    #b = d *255 /maxDist #Plano
    #b = 255 * (d%incColor)/incColor # escalonado
    b = 225 * abs(sin(d*PI/incColor))+30
    stroke(h,s,b)
    point(x,y)

def keyPressed():
    save("hiddenfields.jpg")